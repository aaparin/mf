import 'package:flutter/material.dart';
import 'package:mark_feigin/model/video_model.dart';
import 'package:mark_feigin/services/network_helper.dart';
import 'package:mark_feigin/ui/video_player.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:alert/alert.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../ui/audio_player.dart';
import '../main.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class VideoAppScreen extends StatefulWidget {
  VideoAppScreen({Key? key}) : super(key: key);

  @override
  _VideoAppScreenState createState() => new _VideoAppScreenState();
}

class _VideoAppScreenState extends State<VideoAppScreen> {
  List<int> verticalData = [];
  List<int> horizontalData = [];

  final int increment = 10;

  bool isLoadingVertical = false;
  List<VideoModel> videoList = [];

  final List<String> imgList = [];
  bool? insideVideo = false;

  Future<void> getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    insideVideo = prefs.getBool("insideVideo");
    if (insideVideo == null) {
      insideVideo = false;
    }
    setState(() {});
  }

  Future<List<VideoModel>> getVideo() async {
    String url = "https://backend-dev.feyginlive.net/api/video";
    NetworkHelper networkHelper = NetworkHelper(url: url);
    dynamic data = await networkHelper.getData();
    List<dynamic> getList = data['data'] as List;

    for (int i = 0; i < getList.length; i++) {
      videoList.add(VideoModel(
          id: getList[i]['id'],
          title: getList[i]['title'],
          date: getList[i]['date'],
          small_image: getList[i]['small_image'],
          source_link: getList[i]['source_link'],
          audio_link: getList[i]['audio_link'],
          video_link: getList[i]['video_link']));
    }
    return videoList;
  }

  @override
  void initState() {
    super.initState();
    getVideo().then((value) {
      setState(() {
        videoList = value;
      });
    });
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Видео'),
        backgroundColor: Color.fromRGBO(169, 1, 27, 1),
      ),
      body: videoList.isEmpty
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Scrollbar(
              child: ListView(
                children: [
                  ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: videoList.length,
                    itemBuilder: (context, position) {
                      return DemoItem(videoList[position], insideVideo!);
                    },
                  ),
                ],
              ),
            ),
    );
  }
}

class DemoItem extends ConsumerWidget {
  final VideoModel item;
  final bool insideVideo;

  const DemoItem(
    this.item,
    this.insideVideo, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 32,
                child: Text(
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  item.title,
                  style: TextStyle(color: Colors.black),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    item.date,
                    style: const TextStyle(
                        color: Colors.black, fontStyle: FontStyle.italic),
                  ),
                ),
              ),
              Center(
                  child: CachedNetworkImage(
                fit: BoxFit.cover,
                imageUrl:
                    item.small_image ?? 'http://via.placeholder.com/480x360',
                // placeholder: (context, url) => CircularProgressIndicator(),
                errorWidget: (context, url, error) => Image.asset(
                    'assets/images/error_img.png',
                    fit: BoxFit.cover),
              )),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.black,
                        side: const BorderSide(
                          width: 1.0,
                          color: Colors.white,
                        )),
                    onPressed: () async {
                      final insideVideo = ref.watch(insideVideoProvider);
                      if (insideVideo == true) {
                        if (item.video_link != '') {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ChewieDemo(
                                    link: item.video_link,
                                    title: item.title,
                                  )));
                        } else {
                          Alert(message: 'Видео еще не загружено').show();
                        }
                      } else {
                        final url = item.source_link;
                        if (await canLaunchUrlString(url)) {
                          await launch(url,
                              forceWebView: false, forceSafariVC: false);
                        }
                      }
                    },
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: const [
                        Text('Смотреть'), // <-- Text
                        SizedBox(
                          width: 5,
                        ),
                        Icon(
                          // <-- Icon
                          Icons.remove_red_eye_rounded,
                          size: 24.0,
                        ),
                      ],
                    ),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.black,
                        side: const BorderSide(
                          width: 1.0,
                          color: Colors.white,
                        )),
                    onPressed: () {
                      if (item.audio_link != '') {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => AudioPlayer(
                                  url: item.audio_link,
                                  picture: item.small_image,
                                  name: item.title,
                                )));
                      } else {
                        Alert(message: 'Аудио еще не загружено').show();
                      }
                    },
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: const [
                        Text('Слушать'), // <-- Text
                        SizedBox(
                          width: 5,
                        ),
                        Icon(
                          // <-- Icon
                          Icons.play_circle,
                          size: 24.0,
                        ),
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
