import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';
import '../main.dart';

class ContactsAppScreen extends ConsumerStatefulWidget {
  const ContactsAppScreen({Key? key}) : super(key: key);

  @override
  ConsumerState<ContactsAppScreen> createState() => _ContactsAppScreenState();
}

class _ContactsAppScreenState extends ConsumerState<ContactsAppScreen> {
  SharedPreferences? prefs;
  bool? notifitation;
  bool? insideVideo;
  double? speed;

  @override

  /// set state animation controller
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  Future<void> getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    notifitation = prefs.getBool("notifitation");
    insideVideo = prefs.getBool("insideVideo");
    speed = prefs.getDouble("speed");
    setState(() {});
  }

  Future<bool> saveSwitchState() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("notifitation", notifitation ?? false);
    prefs.setBool("insideVideo", insideVideo ?? false);
    prefs.setDouble("speed", speed ?? 1);

    ref
        .read(insideVideoProvider.notifier)
        .update((state) => insideVideo ?? false);
    ref.read(speedProvider.notifier).update((state) => speed ?? 1);

    return true;
  }

  List<DropdownMenuItem<double>> speedItems = [
    DropdownMenuItem<double>(child: Text("0.75x"), value: 0.75),
    DropdownMenuItem<double>(child: Text("1x"), value: 1),
    DropdownMenuItem<double>(child: Text("1.25x"), value: 1.25),
    DropdownMenuItem<double>(child: Text("1.5x"), value: 1.5),
    DropdownMenuItem<double>(child: Text("1.75x"), value: 1.75),
    DropdownMenuItem<double>(child: Text("2x"), value: 2),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('О приложении'),
        backgroundColor: Color.fromRGBO(169, 1, 27, 1),
      ),
      body: Padding(
        padding: const EdgeInsets.all(18.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const Image(
                image: AssetImage('assets/images/logo.png'),
              ),
              Container(
                child: Text(
                  "Версия 1.0.5 от 19.08.2023",
                ),
              ),
              Divider(),
              ListTile(
                title: const Text('Политика конфиденциальности'),
                onTap: () async {
                  final url = 'https://backend-dev.feyginlive.net/policy';
                  if (await canLaunchUrlString(url)) {
                    await launch(url,
                        forceWebView: false, forceSafariVC: false);
                  }
                },
              ),
              Divider(),
              ListTile(
                title: const Text('Написать нам'),
                onTap: () async {
                  final url = 'mailto:feyginappstore@gmail.com';
                  if (await canLaunchUrlString(url)) {
                    await launch(url,
                        forceWebView: false, forceSafariVC: false);
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
