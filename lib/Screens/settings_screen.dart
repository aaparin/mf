import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:mark_feigin/Screens/contacts.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';

class SettingsAppScreen extends ConsumerStatefulWidget {
  const SettingsAppScreen({Key? key}) : super(key: key);

  @override
  ConsumerState<SettingsAppScreen> createState() => _SettingsAppScreenState();
}

class _SettingsAppScreenState extends ConsumerState<SettingsAppScreen> {
  SharedPreferences? prefs;
  bool? notifitation;
  bool? insideVideo;
  double? speed;

  @override

  /// set state animation controller
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  Future<void> getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    notifitation = prefs.getBool("notifitation");
    insideVideo = prefs.getBool("insideVideo");
    speed = prefs.getDouble("speed");
    setState(() {});
  }

  Future<bool> saveSwitchState() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("notifitation", notifitation ?? false);
    prefs.setBool("insideVideo", insideVideo ?? false);
    prefs.setDouble("speed", speed ?? 1);

    ref
        .read(insideVideoProvider.notifier)
        .update((state) => insideVideo ?? false);
    ref.read(speedProvider.notifier).update((state) => speed ?? 1);

    return true;
  }

  List<DropdownMenuItem<double>> speedItems = [
    DropdownMenuItem<double>(child: Text("0.75x"), value: 0.75),
    DropdownMenuItem<double>(child: Text("1x"), value: 1),
    DropdownMenuItem<double>(child: Text("1.25x"), value: 1.25),
    DropdownMenuItem<double>(child: Text("1.5x"), value: 1.5),
    DropdownMenuItem<double>(child: Text("1.75x"), value: 1.75),
    DropdownMenuItem<double>(child: Text("2x"), value: 2),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Настройки'),
        backgroundColor: Color.fromRGBO(169, 1, 27, 1),
      ),
      body: SafeArea(
        child: Column(children: [
          Card(
            color: Colors.white,
            child: SwitchListTile(
              title: const Text(
                'Разрешить уведомления',
              ),
              value: notifitation ?? false,
              activeColor: Color.fromRGBO(169, 1, 27, 1),
              inactiveTrackColor: Colors.grey,
              onChanged: (bool value) {
                setState(() {
                  notifitation = value;
                  saveSwitchState();
                });
              },
              //secondary: Image.asset("assets/devs.jpg",),
            ),
          ),
          Card(
            color: Colors.white,
            child: SwitchListTile(
              title: const Text(
                'Показывать видео внутри приложения',
              ),
              value: insideVideo ?? false,
              activeColor: Color.fromRGBO(169, 1, 27, 1),
              inactiveTrackColor: Colors.grey,
              onChanged: (bool value) {
                setState(() {
                  insideVideo = value;
                  saveSwitchState();
                });
              },
              //secondary: Image.asset("assets/devs.jpg",),
            ),
          ),
          // ListTile(
          //     title: const Text('Скорость воспроизведения аудио'),
          //     trailing: DropdownButton(
          //       items: speedItems,
          //       value: speed ?? 1,
          //       onChanged: (double? value) {
          //         setState(() {
          //           speed = value;
          //           saveSwitchState();
          //         });
          //       },
          //     )),
          // Divider(color: Colors.grey),
          ListTile(
            title: const Text('О приложении'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return const ContactsAppScreen();
              }));
            },
          )
        ]),
      ),
    );
  }
}
