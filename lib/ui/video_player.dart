import 'dart:io';

import 'package:alert/alert.dart';
import 'package:dio/dio.dart';
// import 'package:downloads_path_provider_28/downloads_path_provider_28.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:chewie/chewie.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:video_player/video_player.dart';
import 'package:wakelock/wakelock.dart';
import 'package:flutter_share/flutter_share.dart';

class ChewieDemo extends StatefulWidget {
  final String? link;
  final String? title;

  const ChewieDemo({super.key, this.link, this.title});

  @override
  State<StatefulWidget> createState() {
    return _ChewieDemoState();
  }
}

class _ChewieDemoState extends State<ChewieDemo> {
  VideoPlayerController? _videoPlayerController1;
  ChewieController? _chewieController;

  Future<void> share() async {
    await FlutterShare.share(
        title: widget.title ?? '',
        text: widget.title ?? '',
        linkUrl: widget.link ?? '');
  }

  downloadFile() async {
    // Map<Permission, PermissionStatus> statuses = await [
    //   Permission.storage,
    // ].request();
    // await Permission.storage.request();
    PermissionStatus storageStatus = await Permission.storage.request();

    if (storageStatus == PermissionStatus.granted) {
      Alert(message: 'Доступ разрешен').show();
    } else {
      Alert(message: 'Доступ запрещен').show();
    }

    // if (statuses[Permission.storage]!.isGranted) {
    //   var dir = await DownloadsPathProvider.downloadsDirectory;
    //   if (dir != null) {
    //     String savename = widget.title ?? 'Video' + '.mp4';
    //     String savePath = dir.path + "/$savename";

    //     try {
    //       Response response = await Dio().get(
    //         widget.link ?? '',
    //         onReceiveProgress: (received, total) {
    //           if (total != -1) {
    //             if (kDebugMode) {
    //               print("${(received / total * 100).toStringAsFixed(0)}%");
    //             }
    //             //you can build progressbar feature too
    //           }
    //         },
    //         options: Options(
    //             responseType: ResponseType.bytes,
    //             followRedirects: false,
    //             validateStatus: (status) {
    //               return status! < 500;
    //             }),
    //       );
    //       print(response.headers);
    //       File file = File(savePath);
    //       var raf = file.openSync(mode: FileMode.write);
    //       // response.data is List<int> type
    //       raf.writeFromSync(response.data);
    //       await raf.close();
    //       Alert(message: 'Видео сохранено в папку загрузки').show();
    //     } on DioError catch (e) {
    //       Alert(message: 'Произошла ошибка скачивания').show();
    //       print(e.message);
    //     }
    //   }
    // } else if (statuses[Permission.storage]!.isPermanentlyDenied) {
    //   var dir = await DownloadsPathProvider.downloadsDirectory;
    //   if (dir != null) {
    //     String savename = "Video.mp4";
    //     String savePath = dir.path + "/$savename";
    //     Response response = await Dio().get(
    //       widget.link ?? '',
    //       onReceiveProgress: (received, total) {
    //         AlertDialog alert = AlertDialog(
    //           content: Row(
    //             children: [
    //               CircularProgressIndicator(),
    //               Container(
    //                   margin: EdgeInsets.only(left: 5),
    //                   child: Text("Идет загрузка. Подождите")),
    //             ],
    //           ),
    //         );
    //         showDialog(
    //           barrierDismissible: false,
    //           context: context,
    //           builder: (BuildContext context) {
    //             return alert;
    //           },
    //         );
    //       },
    //       options: Options(
    //           responseType: ResponseType.bytes,
    //           followRedirects: false,
    //           validateStatus: (status) {
    //             return status! < 500;
    //           }),
    //     );
    //     print(response.headers);
    //     File file = File(savePath);
    //     var raf = file.openSync(mode: FileMode.write);
    //     // response.data is List<int> type
    //     raf.writeFromSync(response.data);
    //     await raf.close();
    //     Alert(message: 'Видео сохранено в папку загрузки').show();
    //     print("Image is saved to download folder.");
    //   }
    // } else {
    //   Alert(message: 'Нет прав на запись').show();
    // }
  }

  var bodyProgress = Container(
    child: Stack(
      children: <Widget>[
        Container(
          alignment: AlignmentDirectional.center,
          decoration: new BoxDecoration(
            color: Colors.white70,
          ),
          child: new Container(
            decoration: new BoxDecoration(
                color: Colors.blue[200],
                borderRadius: new BorderRadius.circular(10.0)),
            width: 300.0,
            height: 200.0,
            alignment: AlignmentDirectional.center,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Center(
                  child: new SizedBox(
                    height: 50.0,
                    width: 50.0,
                    child: new CircularProgressIndicator(
                      value: null,
                      strokeWidth: 7.0,
                    ),
                  ),
                ),
                new Container(
                  margin: const EdgeInsets.only(top: 25.0),
                  child: new Center(
                    child: new Text(
                      "loading.. wait...",
                      style: new TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ),
  );
  @override
  void initState() {
    super.initState();
    _videoPlayerController1 = VideoPlayerController.network(widget.link ?? '');
    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController1!,
      additionalOptions: (context) {
        if (Platform.isAndroid) {
          return <OptionItem>[
            OptionItem(
              onTap: () => share(),
              iconData: Icons.share_sharp,
              title: 'Поделиться',
            ),
            // OptionItem(
            //   onTap: () => downloadFile(),
            //   iconData: Icons.download,
            //   title: 'Cкачать',
            // )
          ];
        } else {
          return <OptionItem>[
            OptionItem(
              onTap: () => share(),
              iconData: Icons.share_sharp,
              title: 'Поделиться',
            )
          ];
        }
      },
      autoPlay: true,
      looping: false,
      aspectRatio: 4 / 2.5,
    );
    Wakelock.enable();
  }

  @override
  void dispose() {
    _videoPlayerController1!.dispose();
    _chewieController!.dispose();
    Wakelock.disable();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text(
          widget.title ?? '',
          style: const TextStyle(
            color: Colors.white,
            fontSize: 14,
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Center(
              child: Chewie(
                controller: _chewieController!,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
