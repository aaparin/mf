import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:mark_feigin/Screens/video_screen.dart';
import 'package:mark_feigin/model/video_model.dart';
import 'package:mark_feigin/ui/audio_player.dart';
import 'package:mark_feigin/ui/video_player.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:alert/alert.dart';
import '../main.dart';

class VideoCarousel extends ConsumerStatefulWidget {
  final List<VideoModel>? videoList;

  const VideoCarousel({super.key, this.videoList});

  @override
  ConsumerState<VideoCarousel> createState() => _VideoCarouselState();
}

class _VideoCarouselState extends ConsumerState<VideoCarousel> {
  bool? insideVideo = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //insideVideo = ref.watch(insideVideoProvider);
  }

  @override
  Widget build(BuildContext context) {
    if (widget.videoList!.isEmpty) {
      return const Center(child: CircularProgressIndicator());
    } else {
      return showVideoBanner(context);
    }
  }

  Widget showVideoBanner(BuildContext context) {
    double? height;
    if (MediaQuery.of(context).orientation == Orientation.portrait) {
      height = 370;
    } else {
      height = 500;
    }
    return Column(
      children: [
        Container(
            decoration: const BoxDecoration(color: Colors.black),
            child: CarouselSlider(
              options:
                  CarouselOptions(enlargeCenterPage: false, height: height),
              items: widget.videoList!
                  .map((item) => Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 32,
                              child: Text(
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                item.title,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  item.date,
                                  style: const TextStyle(
                                      color: Colors.white,
                                      fontStyle: FontStyle.italic),
                                ),
                              ),
                            ),
                            Center(
                                child: CachedNetworkImage(
                              fit: BoxFit.cover,
                              imageUrl: item.small_image ??
                                  'http://via.placeholder.com/480x360',
                              // placeholder: (context, url) => CircularProgressIndicator(),
                              errorWidget: (context, url, error) => Image.asset(
                                  'assets/images/error_img.png',
                                  fit: BoxFit.cover),
                            )),
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      backgroundColor: Colors.black,
                                      side: const BorderSide(
                                        width: 1.0,
                                        color: Colors.white,
                                      )),
                                  onPressed: () async {
                                    final insideVideo =
                                        ref.watch(insideVideoProvider);
                                    if (insideVideo == true) {
                                      if (item.video_link != '') {
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    ChewieDemo(
                                                      link: item.video_link,
                                                      title: item.title,
                                                    )));
                                      } else {
                                        Alert(message: 'Видео еще не загружено')
                                            .show();
                                      }
                                    } else {
                                      final url = item.source_link;
                                      if (await canLaunchUrlString(url)) {
                                        await launch(url,
                                            forceWebView: false,
                                            forceSafariVC: false);
                                      }
                                    }
                                  },
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: const [
                                      Text('Смотреть'), // <-- Text
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Icon(
                                        // <-- Icon
                                        Icons.remove_red_eye_rounded,
                                        size: 24.0,
                                      ),
                                    ],
                                  ),
                                ),
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      backgroundColor: Colors.black,
                                      side: const BorderSide(
                                        width: 1.0,
                                        color: Colors.white,
                                      )),
                                  onPressed: () {
                                    if (item.audio_link != '') {
                                      Navigator.of(context)
                                          .push(MaterialPageRoute(
                                              builder: (context) => AudioPlayer(
                                                    url: item.audio_link,
                                                    picture: item.small_image,
                                                    name: item.title,
                                                  )));
                                    } else {
                                      Alert(message: 'Аудио еще не загружено')
                                          .show();
                                    }
                                  },
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: const [
                                      Text('Слушать'), // <-- Text
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Icon(
                                        // <-- Icon
                                        Icons.play_circle,
                                        size: 24.0,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ))
                  .toList(),
            )),
        ElevatedButton.icon(
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return VideoAppScreen();
            }));
          },
          icon: const Icon(Icons.video_library),
          label: const Text("Показать все видео"),
          style: ElevatedButton.styleFrom(
            backgroundColor: Color.fromRGBO(169, 1, 27, 0.9),
            elevation: 1,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(1.0),
            ),
            textStyle: const TextStyle(
              fontFamily: "Rubik",
              fontSize: 15.0,
              color: Colors.white,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ],
    );
  }
}
