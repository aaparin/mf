import 'package:audio_video_progress_bar/audio_video_progress_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:mark_feigin/main.dart';
import 'package:mark_feigin/ui/page_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AudioPlayer extends ConsumerStatefulWidget {
  final String? url;
  final String? picture;
  final String? name;

  const AudioPlayer({Key? key, this.url, this.picture, this.name})
      : super(key: key);

  @override
  ConsumerState<AudioPlayer> createState() => _AudioPlayerState();
}

class _AudioPlayerState extends ConsumerState<AudioPlayer> {
  late final PageManager _pageManager;
  final List<double> values = [];
  double? speed;

  Future<void> getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    speed = prefs.getDouble("speed");
    ref
        .read(speedProvider.notifier)
        .update((state) => prefs.getDouble("speed"));
    print(speed);
    _pageManager.play(speed);
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _pageManager = PageManager(context, widget.url ?? '', widget.name ?? '',
        widget.picture ?? 'http://via.placeholder.com/480x360');
  }

  @override
  void dispose() {
    _pageManager.dispose();
    super.dispose();
  }

  void handleClick(dynamic value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setDouble("speed", value ?? 1);
    getData();
  }

  List<double> speedItems = [
    0.75,
    1,
    1.25,
    1.5,
    1.75,
    2,
  ];

  @override
  Widget build(BuildContext context) {
    final speed = ref.watch(speedProvider);
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
          backgroundColor: Colors.red,
          automaticallyImplyLeading: false,
          title: Text(widget.name ?? ''),
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: const Icon(
              Icons.close,
              color: Colors.white,
            ),
          ),
          actions: <Widget>[
            PopupMenuButton<dynamic>(
              onSelected: handleClick,
              itemBuilder: (BuildContext context) {
                return speedItems.map((dynamic choice) {
                  return PopupMenuItem<dynamic>(
                    value: choice,
                    child: Row(
                      children: [
                        Icon(
                          Icons.check,
                          color: choice == speed ? Colors.black : Colors.white,
                        ),
                        SizedBox(width: 5),
                        Text(choice.toString())
                      ],
                    ),
                  );
                }).toList();
              },
            ),
          ]),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            flex: 3,
            child: Container(
              child: Center(
                  child: CachedNetworkImage(
                fit: BoxFit.cover,
                imageUrl:
                    widget.picture ?? 'http://via.placeholder.com/480x360',
                // placeholder: (context, url) => CircularProgressIndicator(),
                errorWidget: (context, url, error) => Image.asset(
                    'assets/images/error_img.png',
                    fit: BoxFit.cover),
              )),
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  borderRadius: BorderRadius.circular(30),
                  onTap: () {
                    _pageManager.seek(
                        _pageManager.progressNotifier.value.current -
                            const Duration(seconds: 15));
                  },
                  child: Container(
                    height: 50,
                    width: 50,
                    alignment: Alignment.center,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(30)),
                    child: const Text(
                      '-15 сек',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                const SizedBox(width: 15),
                ValueListenableBuilder<ButtonState>(
                  valueListenable: _pageManager.buttonNotifier,
                  builder: (_, value, __) {
                    switch (value) {
                      case ButtonState.loading:
                        return Container(
                          height: 120,
                          width: 120,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(76)),
                          child: const CircularProgressIndicator(),
                        );
                      case ButtonState.paused:
                        return InkWell(
                          onTap: () => {_pageManager.play(speed)},
                          borderRadius: BorderRadius.circular(76),
                          child: Container(
                            height: 120,
                            width: 120,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(76)),
                            child: IconButton(
                              icon: const Icon(
                                Icons.play_arrow,
                                color: Colors.white,
                                size: 60,
                              ),
                              iconSize: 32.0,
                              onPressed: () => {_pageManager.play(speed)},
                            ),
                          ),
                        );
                      case ButtonState.playing:
                        return InkWell(
                          onTap: _pageManager.pause,
                          borderRadius: BorderRadius.circular(76),
                          child: Container(
                            height: 120,
                            width: 120,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(76)),
                            child: IconButton(
                              icon: const Icon(
                                Icons.pause,
                                color: Colors.white,
                                size: 60,
                              ),
                              iconSize: 32.0,
                              onPressed: _pageManager.pause,
                            ),
                          ),
                        );
                    }
                  },
                ),
                SizedBox(width: 15),
                InkWell(
                  borderRadius: BorderRadius.circular(30),
                  onTap: () {
                    _pageManager.seek(
                      _pageManager.progressNotifier.value.current +
                          const Duration(seconds: 15),
                    );
                  },
                  child: Container(
                    height: 50,
                    width: 50,
                    alignment: Alignment.center,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(30)),
                    child: const Text(
                      '+15 сек',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, right: 20),
                  child: ValueListenableBuilder<ProgressBarState>(
                    valueListenable: _pageManager.progressNotifier,
                    builder: (_, value, __) {
                      return ProgressBar(
                        progress: value.current,
                        buffered: value.buffered,
                        total: value.total,
                        thumbColor: const Color(0xFF89A976),
                        bufferedBarColor: Colors.grey.withOpacity(0.2),
                        baseBarColor: Colors.grey.withOpacity(0.2),
                        progressBarColor: const Color(0xFF89A976),
                        timeLabelTextStyle:
                            const TextStyle(color: Colors.white),
                        onSeek: _pageManager.seek,
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
