import 'package:flutter/material.dart';
import 'package:mark_feigin/model/categories_model.dart';
import 'package:mark_feigin/model/links_model.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../services/network_helper.dart';

class LinksWidget extends StatelessWidget {
  // const LinksWidget({super.key});

  List<CategoriesModel> linksBlocks = [];

  Future<List<CategoriesModel>> getLinks() async {
    List<CategoriesModel> linksBlock = [];
    String url = "https://backend-dev.feyginlive.net/api/links";
    NetworkHelper networkHelper = NetworkHelper(url: url);
    dynamic data = await networkHelper.getData();
    List<dynamic> listList = data['data'] as List;

    for (int i = 0; i < listList.length; i++) {
      List<LinksModel> linksArr = [];

      for (int j = 0; j < listList[i]['links'].length; j++) {
        linksArr.add(LinksModel(
            ico: listList[i]['links'][j]['ico'],
            name: listList[i]['links'][j]['name'],
            url: listList[i]['links'][j]['url']));
      }

      linksBlocks
          .add(CategoriesModel(title: listList[i]['title'], links: linksArr));
    }

    return linksBlocks;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getLinks(),
        builder: (context, snapshot) {
          snapshot.data;
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
            case ConnectionState.active:
              return new Center(
                  child: Container(child: CircularProgressIndicator()));
            case ConnectionState.done:
              return showLinks(context);
          }
        });
  }

  Widget showLinks(context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      itemCount: linksBlocks.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
            width: MediaQuery.of(context).size.width,
            decoration: const BoxDecoration(
              color: Colors.white,
              // borderRadius: BorderRadius.only(topLeft:Radius.circular(0.0),topRight:Radius.circular(0.0),bottomLeft:Radius.circular(10.0),bottomRight:Radius.circular(10.0)),
            ),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 15, 0, 8),
                    child: Text(
                      linksBlocks[index].title,
                      style: TextStyle(
                        fontFamily: "Rubik",
                        fontSize: 20.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: linksBlocks[index].links.length,
                      itemBuilder: (BuildContext context, int linkIndex) {
                        //print(linksBlocks[index].links[linkIndex].ico);
                        return Card(
                          shape: Border(
                            bottom: BorderSide(
                                color: Color.fromRGBO(227, 227, 227, 0.1)),
                          ),
                          child: ListTile(
                            leading:
                                linksBlocks[index].links[linkIndex].ico != ''
                                    ? CachedNetworkImage(
                                        width: 30,
                                        imageUrl: linksBlocks[index]
                                                .links[linkIndex]
                                                .ico ??
                                            'http://via.placeholder.com/30x30',
                                        // placeholder: (context, url) => CircularProgressIndicator(),
                                        // errorWidget: (context, url, error) => Icon(Icons.error),
                                      )
                                    : Image.asset(
                                        'assets/images/pix.png',
                                        width: 30,
                                      ),
                            title: Text(
                                linksBlocks[index].links[linkIndex].name,
                                style: TextStyle(
                                    fontFamily: "Rubik",
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500)),
                            trailing: const Icon(
                              Icons.arrow_forward_ios,
                              size: 14,
                            ),
                            onTap: () async {
                              final url =
                                  linksBlocks[index].links[linkIndex].url;
                              if (await canLaunchUrlString(url)) {
                                await launch(url,
                                    forceWebView: false, forceSafariVC: false);
                              }
                            },
                          ),
                        );
                      })
                ]));
      },
    );
  }
}
