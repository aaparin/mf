import 'dart:ffi';

class VideoModel {
  final int id;
  final String title;
  final String date;
  final String? small_image;
  final String source_link;
  final String? audio_link;
  final String? video_link;

  VideoModel(
      {required this.id,
      required this.title,
      required this.date,
      required this.small_image,
      required this.source_link,
      required this.audio_link,
      required this.video_link});
}
