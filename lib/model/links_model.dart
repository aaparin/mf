
class LinksModel {
  final String ?ico;
  final String name;
  final String url;

  LinksModel({required this.ico, required this.name, required this.url});
}