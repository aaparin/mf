
import 'package:mark_feigin/model/links_model.dart';

class CategoriesModel {
  final String title;

  final List<LinksModel> links;

  CategoriesModel({required this.title, required this.links});
}