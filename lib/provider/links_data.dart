

import 'package:flutter/foundation.dart';
import '../services/network_helper.dart';

class LinksData extends ChangeNotifier {
  List<dynamic> linksBlocks =[];

  Future<void> getLinks() async {
    List<String> linksBlock = [];
    String url = 'https://mf.it-otdel.online/api/links';
    NetworkHelper networkHelper = NetworkHelper(url: url);
    dynamic data = await networkHelper.getData();
    List<dynamic> listList = data['data'] as List;
    for(int i=0;i<listList.length;i++){
      linksBlock.add(listList[i]);
    }
    linksBlocks = linksBlock;
    notifyListeners();
  }
}