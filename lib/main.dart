import 'package:flutter/material.dart';
import 'package:mark_feigin/Screens/settings_screen.dart';
import 'package:mark_feigin/services/network_helper.dart';
import 'package:mark_feigin/ui/linksList.dart';
import 'package:mark_feigin/ui/videoBanner.dart';
import 'package:just_audio_background/just_audio_background.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'model/video_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final speedProvider = StateProvider<double?>((ref) => 1);
final notifitationProvider = StateProvider<bool?>((ref) => false);
final insideVideoProvider = StateProvider<bool?>((ref) => false);

Future<void> main() async {
  await JustAudioBackground.init(
    androidNotificationChannelId: 'com.itcoll.mark_feigin.channel.audio',
    androidNotificationChannelName: 'Audio playback',
    androidNotificationOngoing: true,
  );
  runApp(
    const ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        canvasColor: Colors.white,
      ),
      home: const MyHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends ConsumerStatefulWidget {
  const MyHomePage({super.key});

  @override
  ConsumerState<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends ConsumerState<MyHomePage> {
  bool _pinned = true;
  bool _snap = false;
  bool _floating = false;
  List<VideoModel> videoList = [];

  final List<String> imgList = [];

  Future<List<VideoModel>> getVideo() async {
    String url =
        "https://backend-dev.feyginlive.net/api/video?offset=0&limit=5";
    NetworkHelper networkHelper = NetworkHelper(url: url);
    dynamic data = await networkHelper.getData();
    List<dynamic> getList = data['data'] as List;

    for (int i = 0; i < getList.length; i++) {
      videoList.add(VideoModel(
          id: getList[i]['id'],
          title: getList[i]['title'],
          date: getList[i]['date'],
          small_image: getList[i]['small_image'],
          source_link: getList[i]['source_link'],
          audio_link: getList[i]['audio_link'],
          video_link: getList[i]['video_link']));
    }
    return videoList;
  }

  Future<void> getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    ref
        .read(insideVideoProvider.notifier)
        .update((state) => prefs.getBool("insideVideo"));
    ref
        .read(speedProvider.notifier)
        .update((state) => prefs.getDouble("speed"));
  }

  @override
  void initState() {
    super.initState();
    getData();

    getVideo().then((value) {
      setState(() {
        videoList = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        physics: const BouncingScrollPhysics(),
        slivers: <Widget>[
          SliverAppBar(
            pinned: _pinned,
            snap: _snap,
            floating: _floating,
            expandedHeight: 100.0,
            backgroundColor: const Color.fromRGBO(169, 1, 27, 1),
            shadowColor: Colors.white,
            flexibleSpace: Stack(
              children: const <Widget>[
                Positioned.fill(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                        padding: EdgeInsets.only(bottom: 8.0),
                        child: Image(
                          image: AssetImage('assets/images/banner.png'),
                          width: 200,
                        )),
                  ),
                ),
              ],
            ),
            actions: <Widget>[
              IconButton(
                icon: const Icon(Icons.settings),
                tooltip: 'Настройки',
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return const SettingsAppScreen();
                  }));
                },
              ),
            ],
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              VideoCarousel(videoList: videoList),
              Column(
                children: [
                  Container(
                      decoration: const BoxDecoration(color: Colors.white),
                      child: LinksWidget()),
                ],
              )
            ]),
          ),
        ],
      ),
    );
  }
}
